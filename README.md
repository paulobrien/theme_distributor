**MoDaCo Theme Distributor for Play Store**

The MoDaCo Theme Distributor for Play Store allows you to package a Huawei Emotion UI theme for distribution, surprise suprise, via the Play Store!

This is useful because...

* users are better at installing things from the Play Store than pushing HWT files to the SD card
* it allows you to more easily track download numbers
* it allows you to give your users a description and screenshots before they download

...but best of all...

* Users will be prompted to install the updated HWT file when the app updates from the Play Store, via a notification icon.

This app can be built using Android Studio or via the command line (./gradlew assRel), but there are a few things you need to do first (of course). They are:

* Copy your HWT file to app/src/main/assets - keep the filename the same across versions (important!)
* Copy a preview image of your theme to app/src/main/res/drawable/preview.jpg
* Set the name of your app in app/src/main/res/values/strings.xml (app_name)
* Set the package name (applicationId) and version details of your app in app/build.gradle (note that you will need to increment the version number each time you push to the Play Store).

Before you push, you'll need to sign your app of course.

I hope you find this quick'n'dirty app useful. Enjoy! :)

P.S. The icon used for this theme is a fully licenced icon from icons8.com
