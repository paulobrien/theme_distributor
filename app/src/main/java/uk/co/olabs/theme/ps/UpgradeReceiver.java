package uk.co.olabs.theme.ps;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;

public class UpgradeReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        // Something has been upgrade! Is it us? If so, let's show that notification!
        Uri packageName = intent.getData();
        if (packageName.toString().equals("package:" + context.getPackageName())) {
            showNotification(context);
        }
    }

    private void showNotification(Context context) {
        // Our intent is going to be for our main activity
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
                new Intent(context, MainActivity.class), 0);

        // Can we build it? Yes we can!
        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.mipmap.ic_launcher)
                        .setContentTitle(context.getResources().getString(R.string.app_name) + " " + context.getResources().getString(R.string.theme_updated))
                        .setContentText(context.getResources().getString(R.string.click_here_to_install));
        mBuilder.setContentIntent(contentIntent);
        mBuilder.setAutoCancel(false);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        mNotificationManager.notify(1, mBuilder.build());
    }
}